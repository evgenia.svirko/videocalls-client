const validatePassword = (password: string): boolean =>
  password.length >= 6 && password.length <= 12;

const validateUsername = (username: string): boolean =>
  username.length >= 3 && username.length <= 12;

export const validateMail = (mail: string): boolean => {
  const emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;

  return emailPattern.test(mail);
};

export const validateLoginForm = ({ mail, password }: LoginProps): boolean =>
  validateMail(mail) && validatePassword(password);

export const validateRegisterForm = ({
  mail,
  username,
  password,
}: RegisterProps): boolean =>
  validateMail(mail) &&
  validateUsername(username) &&
  validatePassword(password);
