import React, { ReactNode } from 'react';
import { render } from '@testing-library/react';
import type { RenderOptions } from '@testing-library/react';
import { configureStore } from '@reduxjs/toolkit';
import type { PreloadedState } from '@reduxjs/toolkit';
import { Provider } from 'react-redux';
import { createMemoryHistory } from 'history';
import { Router } from 'react-router-dom';

import { rootReducer, RootState } from '../../store/store';

interface RenderProps extends RenderOptions {
  preloadedState?: PreloadedState<RootState>;
}

export function renderWithProviders(
  ui: React.ReactElement,

  { preloadedState, ...options }: RenderProps = {
    preloadedState: {} as Partial<RootState>,
  }
) {
  const history = createMemoryHistory();

  const mockStore = configureStore({
    reducer: rootReducer,

    preloadedState,
  });

  function Wrapper({ children }: { children: ReactNode }): JSX.Element {
    return (
      <Provider store={mockStore}>
        <Router location={history.location} navigator={history}>
          {children}
        </Router>
      </Provider>
    );
  }

  return {
    history,

    ...render(ui, { wrapper: Wrapper, ...options }),
  };
}

export { renderWithProviders as render };
