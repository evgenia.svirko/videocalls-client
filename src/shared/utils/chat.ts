import { setMessages } from '../../store/slices/chat/chatSlice';
import store from '../../store/store';

const updateChatHistoryIfSameConversationActive = (
  participants: string[],
  usersInCoversation: string[],
  messages: Message[]
): void => {
  const result = participants.every((participantId) =>
    usersInCoversation.includes(participantId)
  );

  if (result) {
    store.dispatch(setMessages(messages));
  }
};

export const updateDirectChatHistoryIfActive = (data: ChatHistory): void => {
  const { participants, messages } = data;

  const receiverId = store.getState().chat.chosenChatDetails?.id;
  const userId = store.getState().auth.user?._id;

  if (receiverId && userId) {
    const usersInCoversation = [receiverId, userId];

    updateChatHistoryIfSameConversationActive(
      participants,
      usersInCoversation,
      messages
    );
  }
};
