import { AuthState } from '../../store/slices/auth/authSlice';

export const authStateMock: AuthState = {
  user: {
    _id: '62c42aed6b9412419928e692',
    mail: 'user@mail.com',
    token:
      'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiI2MmM0MmFlZDZiOTQxMjQxOTkyOGU2OTIiLCJtYWlsIjoidXNlckBtYWlsLmNvbSIsImlhdCI6MTY1Nzg4MjkwMCwiZXhwIjoxNjU3OTY5MzAwfQ.wCK__6NRzP8IY0oCoMhSUx8oEyV_O5CDeX9u5nyWPuA',
    username: 'tom',
  },
};
