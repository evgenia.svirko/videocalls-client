import { FC } from 'react';
import { styled } from '@mui/system';

type Props = {
  username: string;
  large?: boolean;
};

const AvatarPreview = styled('div')({
  height: '42px',
  width: '42px',
  backgroundColor: '#5865f2',
  borderRadius: '42px',
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
  fontSize: '20px',
  fontWeight: '700',
  color: 'white',
});

const Avatar: FC<Props> = ({ username, large }) => {
  return (
    <AvatarPreview style={large ? { height: '80px', width: '80px' } : {}}>
      {username.substring(0, 2)}
    </AvatarPreview>
  );
};

export default Avatar;
