import { fireEvent } from '@testing-library/react';

import { render } from '../utils/test-utils';
import InputWithLabel, { InputWithLabelProps } from './InputWithLabel';

describe('src/shared/Components/InputWithLabel', () => {
  const setValueMock = jest.fn();

  const initProps: InputWithLabelProps = {
    value: 'test value',
    setValue: setValueMock,
    label: 'test label',
    type: 'text',
    placeholder: 'test placeholder',
  };

  it('should render InputWithLabel', () => {
    const { container } = render(<InputWithLabel {...initProps} />);

    expect(container).toMatchSnapshot();
  });

  it('should call setValue fn', () => {
    const { getByPlaceholderText } = render(<InputWithLabel {...initProps} />);

    const input = getByPlaceholderText(initProps.placeholder);
    fireEvent.change(input, { target: { value: 'some value' } });

    expect(setValueMock).toHaveBeenCalledTimes(1);
    expect(setValueMock).toHaveBeenCalledWith('some value');
  });
});
