import Snackbar from '@mui/material/Snackbar';
import Alert from '@mui/material/Alert';

import { useAppSelector, useAppDispatch } from '../../store/store';
import { closeAlert } from '../../store/slices/alert/alertSlice';

const AlertNotification = () => {
  const dispatch = useAppDispatch();
  const { showAlert, alertContent } = useAppSelector((state) => state.alert);

  return (
    <Snackbar
      anchorOrigin={{ vertical: 'bottom', horizontal: 'center' }}
      open={showAlert}
      onClose={() => dispatch(closeAlert())}
      autoHideDuration={3000}
    >
      <Alert severity="info">{alertContent || 'An error occured'}</Alert>
    </Snackbar>
  );
};

export default AlertNotification;
