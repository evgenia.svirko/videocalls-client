import { FC } from 'react';
import Button from '@mui/material/Button';

type Props = {
  label: string;
  disabled?: boolean;
  onClick: () => void;
  additionalStyles?: object;
};

const PrimaryButton: FC<Props> = ({
  label,
  disabled,
  onClick,
  additionalStyles = {},
}) => {
  return (
    <Button
      variant="contained"
      sx={{
        bgColor: '#5865f2',
        color: 'white',
        textTransform: 'none',
        fontSize: '1rem',
        fontWeight: '500',
        width: '100%',
        height: '40px',
      }}
      style={additionalStyles}
      disabled={disabled}
      onClick={onClick}
    >
      {label}
    </Button>
  );
};

export default PrimaryButton;
