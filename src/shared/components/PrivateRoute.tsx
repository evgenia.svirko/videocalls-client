import { Navigate } from 'react-router-dom';

export { PrivateRoute };

type Props = {
  children: JSX.Element;
};

const PrivateRoute = ({ children }: Props) => {
  const user = localStorage.getItem('user');

  if (!user) {
    return <Navigate to="/login" />;
  }

  return children;
};
