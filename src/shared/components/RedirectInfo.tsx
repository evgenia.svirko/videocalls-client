import { FC } from 'react';
import Typography from '@mui/material/Typography';
import { styled } from '@mui/system';

const RedirectText = styled('span')({
  color: '#00aff4',
  fontWeight: '500',
  cursor: 'pointer',
});

type Props = {
  text: string;
  redirectText: string;
  redirectHandler: () => void;
  additionalStyles?: object;
};

const RedirectInfo: FC<Props> = ({
  text,
  redirectText,
  redirectHandler,
  additionalStyles = {},
}) => {
  return (
    <Typography
      sx={{ color: '#72767d' }}
      style={additionalStyles}
      variant="subtitle2"
    >
      {text}
      <RedirectText onClick={redirectHandler}>{redirectText}</RedirectText>
    </Typography>
  );
};

export default RedirectInfo;
