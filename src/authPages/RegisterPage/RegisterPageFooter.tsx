import { FC } from 'react';
import { useNavigate } from 'react-router-dom';
import Tooltip from '@mui/material/Tooltip';

import PrimaryButton from '../../shared/components/PrimaryButton';
import RedirectInfo from '../../shared/components/RedirectInfo';

const formNotValidMessage =
  'Please check if your email is correct, username is between 3 and 12 characters and password is between 6 and 12 characters';

const formValidMessage = 'Press to register';

type Props = {
  handleRegister: () => void;
  isFormValid: boolean;
};

const RegisterPageFooter: FC<Props> = ({ handleRegister, isFormValid }) => {
  const navigate = useNavigate();

  const handleRedirectToLogin = (): void => navigate('/login');

  return (
    <>
      <Tooltip title={!isFormValid ? formNotValidMessage : formValidMessage}>
        <div>
          <PrimaryButton
            label="Register"
            additionalStyles={{ marginTop: '30px' }}
            disabled={!isFormValid}
            onClick={handleRegister}
          />
        </div>
      </Tooltip>
      <RedirectInfo
        text="Already have an account? "
        redirectText="Log in"
        redirectHandler={handleRedirectToLogin}
        additionalStyles={{ marginTop: '5px' }}
      />
    </>
  );
};

export default RegisterPageFooter;
