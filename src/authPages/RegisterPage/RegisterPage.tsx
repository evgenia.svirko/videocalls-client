import { FC, useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import Typography from '@mui/material/Typography';

import AuthBox from '../../shared/components/AuthBox';
import RegisterPageInputs from './RegisterPageInputs';
import RegisterPageFooter from './RegisterPageFooter';
import { validateRegisterForm } from '../../shared/utils/validators';
import { useAppSelector, useAppDispatch } from '../../store/store';
import { register } from '../../store/slices/auth/authOperations';

const RegisterPage: FC = () => {
  const [mail, setMail] = useState('');
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [isFormValid, setIsFormValid] = useState(false);

  const dispatch = useAppDispatch();
  const navigate = useNavigate();
  const { user } = useAppSelector((state) => state.auth);

  useEffect(() => {
    setIsFormValid(validateRegisterForm({ mail, username, password }));
  }, [mail, username, password, setIsFormValid]);

  const handleRegister = (): void => {
    dispatch(register({ mail, password, username }));
    setIsFormValid(true);
  };

  useEffect(() => {
    if (user) {
      localStorage.setItem('user', JSON.stringify(user));
      navigate('/dashboard');
    }
  }, [user]);

  return (
    <AuthBox>
      <>
        <Typography variant="h5" sx={{ color: 'white' }}>
          Create an account
        </Typography>
        <RegisterPageInputs
          mail={mail}
          setMail={setMail}
          username={username}
          setUsername={setUsername}
          password={password}
          setPassword={setPassword}
        />
        <RegisterPageFooter
          isFormValid={isFormValid}
          handleRegister={handleRegister}
        />
      </>
    </AuthBox>
  );
};

export default RegisterPage;
