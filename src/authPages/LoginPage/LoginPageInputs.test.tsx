import { fireEvent } from '@testing-library/react';

import LoginPageInputs, { LoginPageInputsProps } from './LoginPageInputs';
import { render } from '../../shared/utils/test-utils';

describe('src/authPages/LoginPage/LoginPageInputs', () => {
  const setMailMock = jest.fn();
  const setPasswordMock = jest.fn();

  const mailMock = 'test@mail.com';
  const passwordMock = 'qwerty';

  const initProps: LoginPageInputsProps = {
    mail: mailMock,
    setMail: setMailMock,
    password: passwordMock,
    setPassword: setPasswordMock,
  };

  it('should render email input', () => {
    const { getByPlaceholderText } = render(<LoginPageInputs {...initProps} />);

    expect(getByPlaceholderText(/enter email address/i)).toBeInTheDocument();
  });

  it('should render password input', () => {
    const { getByPlaceholderText } = render(<LoginPageInputs {...initProps} />);

    expect(getByPlaceholderText(/enter password/i)).toBeInTheDocument();
  });

  it('should render email value', () => {
    const { getByDisplayValue } = render(<LoginPageInputs {...initProps} />);

    expect(getByDisplayValue(mailMock)).toBeInTheDocument();
  });

  it('should call setMail fn', () => {
    const { getByPlaceholderText } = render(
      <LoginPageInputs {...initProps} mail="" />
    );

    const emailInput = getByPlaceholderText(/enter email address/i);
    const newValue = 'user@mail.com';
    fireEvent.change(emailInput, { target: { value: newValue } });

    expect(setMailMock).toHaveBeenCalledTimes(1);
    expect(setMailMock).toHaveBeenCalledWith(newValue);
  });

  it('should render password value', () => {
    const { getByDisplayValue } = render(<LoginPageInputs {...initProps} />);

    expect(getByDisplayValue(passwordMock)).toBeInTheDocument();
  });

  it('should call setPassword fn', () => {
    const { getByPlaceholderText } = render(
      <LoginPageInputs {...initProps} password="" />
    );

    const passwordInput = getByPlaceholderText(/enter password/i);
    const newValue = '123456';
    fireEvent.change(passwordInput, { target: { value: newValue } });

    expect(setPasswordMock).toHaveBeenCalledTimes(1);
    expect(setPasswordMock).toHaveBeenCalledWith(newValue);
  });
});
