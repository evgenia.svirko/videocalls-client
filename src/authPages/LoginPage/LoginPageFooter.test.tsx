import user from '@testing-library/user-event';

import { render } from '../../shared/utils/test-utils';
import LoginPageFooter, { LoginPageFooterProps } from './LoginPageFooter';

describe('src/authPages/LoginPage/LoginPageFooter', () => {
  const handleLoginMock = jest.fn();

  const initProps: LoginPageFooterProps = {
    handleLogin: handleLoginMock,
    isFormValid: true,
  };

  it('should render log in button', () => {
    const { getByRole } = render(<LoginPageFooter {...initProps} />);

    expect(getByRole('button', { name: /log in/i })).toBeInTheDocument();
  });

  it('should render redirect link', () => {
    const { getByText } = render(<LoginPageFooter {...initProps} />);

    expect(getByText(/create an account/i)).toBeInTheDocument();
  });

  it('should have log in button enabled', () => {
    const { getByRole } = render(<LoginPageFooter {...initProps} />);

    const loginButton = getByRole('button', { name: /log in/i });

    expect(loginButton).toBeEnabled();
  });

  it('should have log in button disabled', () => {
    const { getByRole } = render(
      <LoginPageFooter {...initProps} isFormValid={false} />
    );

    const loginButton = getByRole('button', { name: /log in/i });

    expect(loginButton).toBeDisabled();
  });

  it('should call handleLogin fn', async () => {
    const { getByRole } = render(<LoginPageFooter {...initProps} />);

    const loginButton = getByRole('button', { name: /log in/i });
    await user.click(loginButton);

    expect(handleLoginMock).toHaveBeenCalledTimes(1);
  });

  it('should redirect to register page', async () => {
    const { getByText, history } = render(<LoginPageFooter {...initProps} />);

    const redirectLink = getByText(/create an account/i);
    await user.click(redirectLink);

    expect(history.location.pathname).toBe('/register');
  });
});
