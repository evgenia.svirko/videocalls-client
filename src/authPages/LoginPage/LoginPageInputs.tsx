import { FC } from 'react';

import InputWithLabel from '../../shared/components/InputWithLabel';

export type LoginPageInputsProps = {
  mail: string;
  setMail: React.Dispatch<React.SetStateAction<string>>;
  password: string;
  setPassword: React.Dispatch<React.SetStateAction<string>>;
};

const LoginPageInputs: FC<LoginPageInputsProps> = ({
  mail,
  setMail,
  password,
  setPassword,
}) => {
  return (
    <>
      <InputWithLabel
        value={mail}
        setValue={setMail}
        label="Email"
        type="email"
        placeholder="Enter email address"
      />
      <InputWithLabel
        value={password}
        setValue={setPassword}
        label="Password"
        type="password"
        placeholder="Enter password"
      />
    </>
  );
};

export default LoginPageInputs;
