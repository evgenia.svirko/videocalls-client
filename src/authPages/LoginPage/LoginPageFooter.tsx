import { FC } from 'react';
import { useNavigate } from 'react-router-dom';
import Tooltip from '@mui/material/Tooltip';

import PrimaryButton from '../../shared/components/PrimaryButton';
import RedirectInfo from '../../shared/components/RedirectInfo';

const formNotValidMessage =
  'Please check if your email is correct and password is between 6 and 12 characters';

const formValidMessage = 'Press to log in';

export type LoginPageFooterProps = {
  handleLogin: () => void;
  isFormValid: boolean;
};

const LoginPageFooter: FC<LoginPageFooterProps> = ({
  handleLogin,
  isFormValid,
}) => {
  const navigate = useNavigate();

  const handleRedirectToRegister = (): void => navigate('/register');

  return (
    <>
      <Tooltip title={!isFormValid ? formNotValidMessage : formValidMessage}>
        <div>
          <PrimaryButton
            label="Log in"
            additionalStyles={{ marginTop: '30px' }}
            disabled={!isFormValid}
            onClick={handleLogin}
          />
        </div>
      </Tooltip>
      <RedirectInfo
        text="Need an account? "
        redirectText="Create an account"
        redirectHandler={handleRedirectToRegister}
        additionalStyles={{ marginTop: '5px' }}
      />
    </>
  );
};

export default LoginPageFooter;
