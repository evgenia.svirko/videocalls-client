import { FC, useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import Typography from '@mui/material/Typography';

import AuthBox from '../../shared/components/AuthBox';
import { validateLoginForm } from '../../shared/utils/validators';
import LoginPageFooter from './LoginPageFooter';
import LoginPageInputs from './LoginPageInputs';
import { useAppSelector, useAppDispatch } from '../../store/store';
import { login } from '../../store/slices/auth/authOperations';

const LoginPage: FC = () => {
  const [mail, setMail] = useState('');
  const [password, setPassword] = useState('');
  const [isFormValid, setIsFormValid] = useState(false);

  const dispatch = useAppDispatch();
  const navigate = useNavigate();
  const { user } = useAppSelector((state) => state.auth);

  console.log('user', user);

  useEffect(() => {
    setIsFormValid(validateLoginForm({ mail, password }));
  }, [mail, password, setIsFormValid]);

  const handleLogin = (): void => {
    dispatch(login({ mail, password }));
  };

  useEffect(() => {
    if (user) {
      localStorage.setItem('user', JSON.stringify(user));
      navigate('/dashboard');
    }
  }, [user]);

  return (
    <AuthBox>
      <>
        <Typography variant="h5" sx={{ color: 'white' }}>
          Welcome back!
        </Typography>

        <LoginPageInputs
          mail={mail}
          setMail={setMail}
          password={password}
          setPassword={setPassword}
        />
        <LoginPageFooter isFormValid={isFormValid} handleLogin={handleLogin} />
      </>
    </AuthBox>
  );
};

export default LoginPage;
