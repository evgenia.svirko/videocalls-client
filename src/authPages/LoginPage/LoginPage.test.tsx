import { fireEvent } from '@testing-library/react';
import * as router from 'react-router';

import { authStateMock } from '../../shared/mocks/authStateMock';
import { render } from '../../shared/utils/test-utils';
import { login } from '../../store/slices/auth/authOperations';
import LoginPage from './LoginPage';

const navigate = jest.fn();

describe('src/authPages/LoginPage/LoginPage', () => {
  beforeEach(() => {
    jest.spyOn(router, 'useNavigate').mockImplementation(() => navigate);
  });

  it('should render welcome text', () => {
    const { getByText } = render(<LoginPage />);

    expect(getByText(/welcome back/i)).toBeInTheDocument();
  });

  it('should redirect to dashboard if the user exists', () => {
    render(<LoginPage />, { preloadedState: { auth: authStateMock } });

    expect(navigate).toHaveBeenCalledWith('/dashboard');
  });

  it('should get user from localStorage', () => {
    render(<LoginPage />, { preloadedState: { auth: authStateMock } });

    expect(localStorage.getItem('user')).toBe(
      JSON.stringify(authStateMock.user)
    );
  });
});

describe('user login flow', () => {
  it('should login user', async () => {
    jest.spyOn(router, 'useNavigate').mockImplementation(() => navigate);

    expect(localStorage.getItem('user')).toBeNull();

    const { getByPlaceholderText, getByRole } = render(<LoginPage />);

    const emailInput = getByPlaceholderText(/enter email address/i);
    const mail = 'user@mail.com';
    fireEvent.change(emailInput, { target: { value: mail } });

    const passwordInput = getByPlaceholderText(/enter password/i);
    const password = '123456';
    fireEvent.change(passwordInput, { target: { value: password } });

    const loginButton = getByRole('button', { name: /log in/i });
    fireEvent.click(loginButton);

    const dispatch = jest.fn();
    const mockState = {};

    const loginThunk = login({ mail, password });
    await loginThunk(dispatch, () => mockState, undefined);

    const { calls } = dispatch.mock;

    expect(calls).toHaveLength(2);
    expect(calls[0][0].type).toBe('auth/login/pending');
    expect(calls[1][0].type).toBe('auth/login/fulfilled');
    expect(calls[1][0].payload).toStrictEqual(authStateMock.user);

    expect(localStorage.getItem('user')).toBe(
      JSON.stringify(authStateMock.user)
    );

    expect(navigate).toHaveBeenCalledWith('/dashboard');
  });
});
