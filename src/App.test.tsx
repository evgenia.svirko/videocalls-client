import { configureStore } from '@reduxjs/toolkit';
import { Provider } from 'react-redux';
import { render } from '@testing-library/react';

import App from './App';
import { rootReducer } from './store/store';

const mockStore = configureStore({
  reducer: rootReducer,

  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware({ serializableCheck: false }),

  preloadedState: {},
});

describe('src/App', () => {
  it('renders login welcome message', () => {
    const { getByText } = render(
      <Provider store={mockStore}>
        <App />
      </Provider>
    );

    const text = getByText(/welcome back/i);

    expect(text).toBeInTheDocument();
  });
});
