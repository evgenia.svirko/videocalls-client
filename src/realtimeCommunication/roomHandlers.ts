import store from '../store/store';
import {
  openRoom,
  setActiveRooms,
  setIsUserJoinedOnlyWithAudio,
  setLocalStream,
  setRemoteStreams,
  setRoomDetails,
  setScreenSharingStream,
} from '../store/slices/room/roomSlice';
import {
  emitCreateNewRoom,
  emitJoinRoom,
  emitleaveRoom,
} from './socketConnection';
import { closeAllConnections, getLocalStreamPreview } from './webRTCHandler';

export const createNewRoom = (): void => {
  const successCalbackFunc = (): void => {
    store.dispatch(
      openRoom({
        isUserInRoom: true,
        isUserRoomCreator: true,
      })
    );

    const { audioOnly } = store.getState().room;

    store.dispatch(setIsUserJoinedOnlyWithAudio(audioOnly));

    emitCreateNewRoom();
  };

  const { audioOnly } = store.getState().room;

  getLocalStreamPreview(audioOnly, successCalbackFunc);
};

export const updateActiveRooms = (data: { activeRooms: Room[] }): void => {
  const { activeRooms } = data;

  const { friends } = store.getState().friends;
  const rooms: ActiveRoom[] = [];

  const userId = store.getState().auth.user?._id;

  activeRooms.forEach((room) => {
    const isRoomCreatedByMe = room.roomCreator.userId === userId;

    if (isRoomCreatedByMe) {
      rooms.push({ ...room, creatorUsername: 'Me' });
    } else {
      friends.forEach((friend) => {
        if (friend.id === room.roomCreator.userId) {
          rooms.push({ ...room, creatorUsername: friend.username });
        }
      });
    }
  });

  store.dispatch(setActiveRooms(rooms));
};

export const joinRoom = (room: ActiveRoom): void => {
  const successCalbackFunc = (): void => {
    store.dispatch(setRoomDetails(room));
    store.dispatch(
      openRoom({
        isUserInRoom: true,
        isUserRoomCreator: false,
      })
    );
    emitJoinRoom(room.roomId);

    const audioOnly = store.getState().room.audioOnly;

    store.dispatch(setIsUserJoinedOnlyWithAudio(audioOnly));
  };
  const audioOnly = store.getState().room.audioOnly;

  getLocalStreamPreview(audioOnly, successCalbackFunc);
};

export const leaveRoom = (): void => {
  const roomId = store.getState().room.roomDetails?.roomId || '';

  const { localStream } = store.getState().room;

  if (localStream) {
    localStream.getTracks().forEach((track) => track.stop());
    store.dispatch(setLocalStream(null));
  }

  const { screenSharingStream } = store.getState().room;

  if (screenSharingStream) {
    screenSharingStream.getTracks().forEach((track) => track.stop());
    store.dispatch(setScreenSharingStream(null));
  }

  store.dispatch(setRemoteStreams([]));
  closeAllConnections();

  emitleaveRoom(roomId);

  store.dispatch(setRoomDetails(null));
  store.dispatch(
    openRoom({
      isUserInRoom: false,
      isUserRoomCreator: false,
    })
  );
};
