import { io, Socket } from 'socket.io-client';

import {
  setFriends,
  setOnlineUsers,
  setPendingInvitations,
} from '../store/slices/friends/friendsSlice';
import store from '../store/store';
import { updateDirectChatHistoryIfActive } from '../shared/utils/chat';
import { setRoomDetails } from '../store/slices/room/roomSlice';
import { updateActiveRooms } from './roomHandlers';
import {
  handleParticipantLeftRoom,
  handleSignalingData,
  prepareNewPeerConnection,
} from './webRTCHandler';

let socket: Socket | null = null;

export const connectWithSocketServer = (userDetails: AuthResponse): void => {
  socket = io('https://dry-river-06701.herokuapp.com', {
    auth: { token: userDetails.token },
  });

  socket.on('friends-invitations', ({ pendingInvitations }) =>
    store.dispatch(setPendingInvitations(pendingInvitations))
  );

  socket.on('friends-list', ({ friends }) =>
    store.dispatch(setFriends(friends))
  );

  socket.on('online-users', ({ onlineUsers }) =>
    store.dispatch(setOnlineUsers(onlineUsers))
  );

  socket.on('direct-chat-history', (data) =>
    updateDirectChatHistoryIfActive(data)
  );

  socket.on('room-create', ({ roomDetails }) =>
    store.dispatch(setRoomDetails(roomDetails))
  );

  socket.on('active-rooms', (data) => updateActiveRooms(data));

  socket.on('connect-prepare', ({ connectUserSocketId }) => {
    prepareNewPeerConnection(connectUserSocketId, false);

    socket?.emit('connect-init', { connectUserSocketId });
  });

  socket.on('connect-init', ({ connectUserSocketId }) =>
    prepareNewPeerConnection(connectUserSocketId, true)
  );

  socket.on('connect-signal', (data) => {
    handleSignalingData(data);
  });

  socket.on('room-participant-left', (data) => {
    handleParticipantLeftRoom(data);
  });
};

export const sendDirectMessage = (data: SendMessageProps): void => {
  socket?.emit('direct-message', data);
};

export const getDirectChatHistory = (data: GetChatHistory): void => {
  socket?.emit('direct-chat-history', data);
};

export const emitCreateNewRoom = (): void => {
  socket?.emit('room-create');
};

export const emitJoinRoom = (roomId: string): void => {
  socket?.emit('room-join', { roomId });
};

export const emitleaveRoom = (roomId: string): void => {
  socket?.emit('room-leave', { roomId });
};

export const signalPeerData = (data: SignalData): void => {
  socket?.emit('connect-signal', data);
};
