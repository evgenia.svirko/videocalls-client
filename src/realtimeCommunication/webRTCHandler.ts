import Peer from 'simple-peer';

import {
  setLocalStream,
  setRemoteStreams,
} from '../store/slices/room/roomSlice';
import store from '../store/store';
import { signalPeerData } from './socketConnection';

const peers: Record<string, Peer.Instance> = {};

const getConfiguration = (): { iceServers: { urls: string }[] } | undefined => {
  const turnIceServers = null;

  if (turnIceServers) {
    // TODO use TURN server credentials
  } else {
    console.warn('Using only STUN server');

    return { iceServers: [{ urls: 'stun:stun.l.google.com:19302' }] };
  }
};

const onlyAudioConstraints = { audio: true, video: false };

const defaultConstraints = { video: true, audio: true };

export const getLocalStreamPreview = (
  onlyAudio: boolean,
  callbackFunc: () => void
): void => {
  const constraints = onlyAudio ? onlyAudioConstraints : defaultConstraints;

  navigator.mediaDevices
    .getUserMedia(constraints)
    .then((stream) => {
      store.dispatch(setLocalStream(stream));
      callbackFunc();
    })
    .catch((err) => {
      console.log(err);
      console.error('Cannot get an access to local stream');
    });
};

const addNewRemoteStream = (remoteStream: RemoteStream): void => {
  const { remoteStreams } = store.getState().room;
  const newRemoteStreams = [...remoteStreams, remoteStream];

  store.dispatch(setRemoteStreams(newRemoteStreams));
};

export const prepareNewPeerConnection = (
  connectUserSocketId: string,
  isInitiator: boolean
): void => {
  const { localStream } = store.getState().room;

  peers[connectUserSocketId] = new Peer({
    initiator: isInitiator,
    config: getConfiguration(),
    stream: localStream || undefined,
  });

  peers[connectUserSocketId].on('signal', (data) => {
    const signalData = {
      signal: data,
      connectUserSocketId,
    };

    signalPeerData(signalData);
  });

  peers[connectUserSocketId].on('stream', (remoteStream: MediaStream) =>
    addNewRemoteStream({ remoteStream, connectUserSocketId })
  );
};

export const handleSignalingData = ({
  connectUserSocketId,
  signal,
}: SignalData): void => {
  if (peers[connectUserSocketId]) {
    peers[connectUserSocketId].signal(signal);
  }
};

const removePeer = (connectUserSocketId: string): void => {
  if (peers[connectUserSocketId]) {
    peers[connectUserSocketId].destroy();
    delete peers[connectUserSocketId];
  }
};

export const closeAllConnections = (): void => {
  Object.entries(peers).forEach((mappedObject) => {
    const connectUserSocketId = mappedObject[0];

    removePeer(connectUserSocketId);
  });
};

export const handleParticipantLeftRoom = ({
  connectUserSocketId,
}: {
  connectUserSocketId: string;
}): void => {
  removePeer(connectUserSocketId);

  const { remoteStreams } = store.getState().room;

  const newRemoteStreams = remoteStreams.filter(
    (remoteStream) => remoteStream.connectUserSocketId !== connectUserSocketId
  );

  store.dispatch(setRemoteStreams(newRemoteStreams));
};

export const switchOutgoingTracks = (stream: MediaStream): void => {
  for (const socket_id in peers) {
    const peersStreams = (peers[socket_id] as unknown as Peer.Options)
      .streams?.[0];

    if (peersStreams) {
      for (const index in peersStreams.getTracks()) {
        for (const index2 in stream.getTracks()) {
          if (
            peersStreams.getTracks()[index].kind ===
            stream.getTracks()[index2].kind
          ) {
            peers[socket_id].replaceTrack(
              peersStreams.getTracks()[index],
              stream.getTracks()[index2],
              peersStreams
            );
            break;
          }
        }
      }
    }
  }
};
