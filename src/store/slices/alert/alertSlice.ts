import { createSlice } from '@reduxjs/toolkit';
import { sendInvitation } from '../friends/friendsOperations';

interface AlertState {
  showAlert: boolean;
  alertContent: string | null;
}

const initialState: AlertState = {
  showAlert: false,
  alertContent: null,
};

const alertSlice = createSlice({
  name: 'alert',
  initialState,
  reducers: {
    openAlert(state, { payload }) {
      state.showAlert = true;
      state.alertContent = payload;
    },

    closeAlert(state) {
      state.showAlert = false;
      state.alertContent = null;
    },
  },

  extraReducers: (builder) => {
    builder.addCase(sendInvitation.fulfilled, (state) => {
      state.showAlert = true;
      state.alertContent = 'Invitation has been sent';
    });

    builder.addMatcher(
      (action) =>
        action.type.endsWith('/rejected') ||
        action.type.endsWith('acceptInvitation/fulfilled') ||
        action.type.endsWith('rejectInvitation/fulfilled'),
      (state, { payload }) => {
        state.showAlert = true;
        state.alertContent = payload || null;
      }
    );
  },
});

export const { openAlert, closeAlert } = alertSlice.actions;
export default alertSlice.reducer;
