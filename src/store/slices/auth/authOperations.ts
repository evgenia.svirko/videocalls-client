import { createAsyncThunk } from '@reduxjs/toolkit';
import axios, { AxiosError } from 'axios';

export const login = createAsyncThunk<
  AuthResponse,
  LoginProps,
  {
    rejectValue: string;
  }
>('auth/login', async (data, { rejectWithValue }) => {
  try {
    const response = await axios.post<AuthResponse>('/auth/login', data);

    return response.data;
  } catch (error) {
    if (!(error as AxiosError<string>).response) {
      throw error;
    }

    return rejectWithValue((error as AxiosError<string>).response?.data || '');
  }
});

export const register = createAsyncThunk<
  AuthResponse,
  RegisterProps,
  {
    rejectValue: string;
  }
>('auth/register', async (data, { rejectWithValue }) => {
  try {
    const response = await axios.post<AuthResponse>('/auth/register', data);

    return response.data;
  } catch (error) {
    if (!(error as AxiosError<string>).response) {
      throw error;
    }

    return rejectWithValue((error as AxiosError<string>).response?.data || '');
  }
});
