import { createSlice, PayloadAction } from '@reduxjs/toolkit';

import { login, register } from './authOperations';

export interface AuthState {
  user: AuthResponse | null;
}

const initialState: AuthState = {
  user: null,
};

const authSlice = createSlice({
  name: 'auth',
  initialState,
  reducers: {
    setUserDetails: (state, action: PayloadAction<AuthResponse | null>) => {
      state.user = action.payload;
    },
  },
  extraReducers: (builder) => {
    // LOGIN
    builder.addCase(login.pending, (state) => {
      state.user = null;
    });

    builder.addCase(login.fulfilled, (state, { payload }) => {
      state.user = payload;
    });

    builder.addCase(login.rejected, (state) => {
      state.user = null;
    });

    // REGISTER
    builder.addCase(register.pending, (state) => {
      state.user = null;
    });

    builder.addCase(register.fulfilled, (state, { payload }) => {
      state.user = payload;
    });

    builder.addCase(register.rejected, (state) => {
      state.user = null;
    });
  },
});

export const { setUserDetails } = authSlice.actions;
export default authSlice.reducer;
