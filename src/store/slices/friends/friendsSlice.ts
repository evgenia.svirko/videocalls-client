import { createSlice, PayloadAction } from '@reduxjs/toolkit';

interface FriendsState {
  friends: Friend[];
  pendingInvitations: Invitation[];
  onlineUsers: OnlineUser[];
}

const initialState: FriendsState = {
  friends: [],
  pendingInvitations: [],
  onlineUsers: [],
};

const friendsSlice = createSlice({
  name: 'friends',
  initialState,
  reducers: {
    setPendingInvitations(state, { payload }: PayloadAction<Invitation[]>) {
      return {
        ...state,
        pendingInvitations: payload,
      };
    },

    setFriends(state, { payload }: PayloadAction<Friend[]>) {
      return {
        ...state,
        friends: payload,
      };
    },

    setOnlineUsers(state, { payload }: PayloadAction<OnlineUser[]>) {
      return {
        ...state,
        onlineUsers: payload,
      };
    },
  },
});

export const { setPendingInvitations, setFriends, setOnlineUsers } =
  friendsSlice.actions;
export default friendsSlice.reducer;
