import { createAsyncThunk } from '@reduxjs/toolkit';
import axios, { AxiosError } from 'axios';

import { checkResponseCode } from '../../../api';

export const sendInvitation = createAsyncThunk<
  Invitation,
  InvitationProps,
  {
    rejectValue: string;
  }
>('friends/sendInvitation', async (data, { rejectWithValue }) => {
  try {
    const response = await axios.post<Invitation>(
      '/friend-invitation/invite',
      data
    );

    return response.data;
  } catch (error) {
    if (!(error as AxiosError<string>).response) {
      throw error;
    }
    checkResponseCode(error as AxiosError<string>);

    return rejectWithValue((error as AxiosError<string>).response?.data || '');
  }
});

export const acceptInvitation = createAsyncThunk<
  string,
  InviteDecisionProps,
  {
    rejectValue: string;
  }
>('friends/acceptInvitation', async (data, { rejectWithValue }) => {
  try {
    const response = await axios.post<string>(
      '/friend-invitation/accept',
      data
    );

    return response.data;
  } catch (error) {
    if (!(error as AxiosError<string>).response) {
      throw error;
    }

    return rejectWithValue((error as AxiosError<string>).response?.data || '');
  }
});

export const rejectInvitation = createAsyncThunk<
  string,
  InviteDecisionProps,
  {
    rejectValue: string;
  }
>('friends/rejectInvitation', async (data, { rejectWithValue }) => {
  try {
    const response = await axios.post<string>(
      '/friend-invitation/reject',
      data
    );

    return response.data;
  } catch (error) {
    if (!(error as AxiosError<string>).response) {
      throw error;
    }

    return rejectWithValue((error as AxiosError<string>).response?.data || '');
  }
});
