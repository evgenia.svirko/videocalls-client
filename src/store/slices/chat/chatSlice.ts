import { createSlice, PayloadAction } from '@reduxjs/toolkit';

interface ChatState {
  chosenChatDetails: ChatDetails | null;
  chatType: ChatType | null;
  messages: Message[];
}

const initialState: ChatState = {
  chosenChatDetails: null,
  chatType: null,
  messages: [],
};

const chatSlice = createSlice({
  name: 'chat',
  initialState,
  reducers: {
    setChosenChatDetails(state, { payload }: PayloadAction<ChatDetails>) {
      return {
        ...state,
        chosenChatDetails: payload,
      };
    },

    setMessages(state, { payload }: PayloadAction<Message[]>) {
      return {
        ...state,
        messages: payload,
      };
    },

    setChatType(state, { payload }: PayloadAction<ChatType>) {
      return {
        ...state,
        chatType: payload,
      };
    },
  },
});

export const { setChosenChatDetails, setMessages, setChatType } =
  chatSlice.actions;
export default chatSlice.reducer;
