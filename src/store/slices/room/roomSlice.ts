import { createSlice, PayloadAction } from '@reduxjs/toolkit';

interface RoomState {
  isUserInRoom: boolean;
  isUserRoomCreator: boolean;
  roomDetails: Room | null;
  activeRooms: ActiveRoom[];
  localStream: MediaStream | null;
  remoteStreams: RemoteStream[];
  audioOnly: boolean;
  screenSharingStream: MediaStream | null;
  isScreenSharingActive: boolean;
  isUserJoinedWithOnlyAudio: boolean;
}

const initialState: RoomState = {
  isUserInRoom: false,
  isUserRoomCreator: false,
  roomDetails: null,
  activeRooms: [],
  localStream: null,
  remoteStreams: [],
  audioOnly: false,
  screenSharingStream: null,
  isScreenSharingActive: false,
  isUserJoinedWithOnlyAudio: false,
};

const roomSlice = createSlice({
  name: 'room',
  initialState,
  reducers: {
    openRoom(
      state,
      {
        payload,
      }: PayloadAction<{ isUserInRoom: boolean; isUserRoomCreator: boolean }>
    ) {
      return {
        ...state,
        isUserInRoom: payload.isUserInRoom,
        isUserRoomCreator: payload.isUserRoomCreator,
      };
    },

    setRoomDetails(state, { payload }: PayloadAction<Room | null>) {
      return {
        ...state,
        roomDetails: payload,
      };
    },

    setActiveRooms(state, { payload }: PayloadAction<ActiveRoom[]>) {
      return {
        ...state,
        activeRooms: payload,
      };
    },

    setLocalStream(state, { payload }: PayloadAction<MediaStream | null>) {
      return {
        ...state,
        localStream: payload,
      };
    },

    setIsUserJoinedOnlyWithAudio(state, { payload }: PayloadAction<boolean>) {
      return {
        ...state,
        isUserJoinedWithOnlyAudio: payload,
      };
    },

    setRemoteStreams(state, { payload }: PayloadAction<RemoteStream[]>) {
      return {
        ...state,
        remoteStreams: payload,
      };
    },

    setAudioOnly(state, { payload }: PayloadAction<boolean>) {
      return {
        ...state,
        audioOnly: payload,
      };
    },

    setScreenSharingStream(
      state,
      { payload }: PayloadAction<MediaStream | null>
    ) {
      return {
        ...state,
        screenSharingStream: payload,
        isScreenSharingActive: !!payload,
      };
    },
  },
});

export const {
  openRoom,
  setRoomDetails,
  setActiveRooms,
  setLocalStream,
  setIsUserJoinedOnlyWithAudio,
  setRemoteStreams,
  setAudioOnly,
  setScreenSharingStream,
} = roomSlice.actions;
export default roomSlice.reducer;
