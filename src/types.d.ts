type LoginProps = {
  mail: string;
  password: string;
};

type RegisterProps = {
  mail: string;
  username: string;
  password: string;
};

type AuthResponse = {
  _id: string;
  mail: string;
  token: string;
  username: string;
};

type InvitationProps = {
  targetMail: string;
};

type Invitation = {
  _id: string;
  receiverId: string;
  senderId: {
    mail: string;
    username: string;
    _id: string;
  };
};

type InviteDecisionProps = {
  id: string;
};

type Friend = {
  id: string;
  username: string;
  mail: string;
};

type OnlineUser = {
  socketId: string;
  userId: string;
};

type ChatType = 'DIRECT' | 'GROUP';

type ChatDetails = {
  id: string;
  name: string;
};

type Message = {
  _id: string;
  content: string;
  sameAuthor: boolean;
  date: string;
  sameDay: boolean;
  author: {
    _id: string;
    username: string;
  };
};

type SendMessageProps = {
  receiverUserId: string;
  content: string;
};

type GetChatHistory = {
  receiverUserId: string;
};

type ChatHistory = {
  messages: Message[];
  participants: string[];
};

type Room = {
  participants: OnlineUser[];
  roomCreator: OnlineUser;
  roomId: string;
};

type ActiveRoom = {
  participants: OnlineUser[];
  roomCreator: OnlineUser;
  roomId: string;
  creatorUsername: string;
};

type SignalData = {
  signal: Peer.SignalData;
  connectUserSocketId: string;
};

type RemoteStream = {
  remoteStream: MediaStream;
  connectUserSocketId: string;
};
