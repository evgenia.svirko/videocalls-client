import { useDispatch } from 'react-redux';

import Dashboard from './Dashboard';
import { render } from '../shared/utils/test-utils';
import { authStateMock } from '../shared/mocks/authStateMock';

jest.mock('react-redux', () => ({
  ...jest.requireActual('react-redux'),

  useDispatch: jest.fn(),
}));

describe('src/Dashboard/Dashboard', () => {
  it('should render Dashboard', () => {
    const { getByText } = render(<Dashboard />, {
      preloadedState: { auth: authStateMock },
    });

    expect(
      getByText(/to start chatting choose conversation/i)
    ).toBeInTheDocument();
  });

  it('should clear localStorage if there is no user', () => {
    render(<Dashboard />, { preloadedState: { auth: { user: null } } });

    const userDetails = localStorage.getItem('user');

    expect(userDetails).toBeNull();
  });

  it('should dispatch setUserDetails action', () => {
    localStorage.setItem('user', JSON.stringify(authStateMock));

    const mockDispatch = jest.fn();

    (useDispatch as jest.MockedFunction<typeof useDispatch>).mockReturnValue(
      mockDispatch
    );

    render(<Dashboard />);

    expect(mockDispatch).toHaveBeenCalledTimes(1);
    expect(mockDispatch).toHaveBeenCalledWith({
      payload: { user: authStateMock.user },
      type: 'auth/setUserDetails',
    });
  });
});
