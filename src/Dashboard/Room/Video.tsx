import { FC, useEffect, useRef } from 'react';
import { styled } from '@mui/system';

const MainContainer = styled('div')({
  height: '50%',
  width: '50%',
  backgroundColor: 'black',
  borderRadius: '8px',
});

const VideoEl = styled('video')({
  width: '100%',
  height: '100%',
});

type Props = {
  stream: MediaStream | null;
  isLocalStream?: boolean;
};

const Video: FC<Props> = ({ stream, isLocalStream = false }) => {
  const videoRef = useRef<HTMLVideoElement | null>(null);

  useEffect(() => {
    const video = videoRef.current;

    if (video) {
      video.srcObject = stream;

      video.onloadedmetadata = () => {
        video.play();
      };
    }
  }, [stream]);

  return (
    <MainContainer>
      <VideoEl ref={videoRef} autoPlay muted={isLocalStream ? true : false} />
    </MainContainer>
  );
};

export default Video;
