import { styled } from '@mui/system';

import Video from './Video';
import { useAppSelector } from '../../store/store';

const MainContainer = styled('div')({
  height: '85%',
  width: '100%',
  display: 'flex',
  flexWrap: 'wrap',
});

const VideosContainer = () => {
  const { localStream, remoteStreams, screenSharingStream } = useAppSelector(
    (state) => state.room
  );

  return (
    <MainContainer>
      <Video
        stream={screenSharingStream ? screenSharingStream : localStream}
        isLocalStream
      />
      {remoteStreams.map(({ remoteStream }) => (
        <Video stream={remoteStream} key={remoteStream.id} />
      ))}
    </MainContainer>
  );
};

export default VideosContainer;
