import { styled } from '@mui/system';

import MicButton from './MicButton';
import ScreenShareButton from './ScreenShareButton';
import CloseRoomButton from './CloseRoomButton';
import CameraButton from './CameraButton';
import { useAppSelector } from '../../../store/store';

const MainContainer = styled('div')({
  height: '15%',
  width: '100%',
  backgroundColor: '#5865f2',
  borderTopLeftRadius: '8px',
  borderTopRightRadius: '8px',
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
});

const RoomButtons = () => {
  const { localStream, isUserJoinedWithOnlyAudio } = useAppSelector(
    (state) => state.room
  );

  return (
    <MainContainer>
      {!isUserJoinedWithOnlyAudio && <ScreenShareButton />}
      <MicButton localStream={localStream} />
      <CloseRoomButton />
      {!isUserJoinedWithOnlyAudio && <CameraButton localStream={localStream} />}
    </MainContainer>
  );
};

export default RoomButtons;
