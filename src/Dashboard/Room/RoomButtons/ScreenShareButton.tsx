import IconButton from '@mui/material/IconButton';
import ScreenShareIcon from '@mui/icons-material/ScreenShare';
import StopScreenShareIcon from '@mui/icons-material/StopScreenShare';

import { useAppDispatch, useAppSelector } from '../../../store/store';
import { setScreenSharingStream } from '../../../store/slices/room/roomSlice';
import { switchOutgoingTracks } from '../../../realtimeCommunication/webRTCHandler';

const constraints = {
  audio: false,
  video: true,
};

const ScreenShareButton = () => {
  const dispatch = useAppDispatch();
  const { localStream, isScreenSharingActive, screenSharingStream } =
    useAppSelector((state) => state.room);

  const handleScreenShareToggle = async (): Promise<void> => {
    if (!isScreenSharingActive) {
      let stream = null;

      try {
        stream = await navigator.mediaDevices.getDisplayMedia(constraints);
      } catch (err) {
        console.log(
          'error occured when trying to get an access to screen share stream'
        );
      }

      if (stream) {
        dispatch(setScreenSharingStream(stream));
        switchOutgoingTracks(stream);
      }
    } else {
      localStream && switchOutgoingTracks(localStream);
      screenSharingStream?.getTracks().forEach((t) => t.stop());
      dispatch(setScreenSharingStream(null));
    }
  };

  return (
    <IconButton onClick={handleScreenShareToggle} style={{ color: 'white' }}>
      {isScreenSharingActive ? <StopScreenShareIcon /> : <ScreenShareIcon />}
    </IconButton>
  );
};

export default ScreenShareButton;
