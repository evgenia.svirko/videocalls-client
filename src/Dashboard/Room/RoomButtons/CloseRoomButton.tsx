import IconButton from '@mui/material/IconButton';
import CloseIcon from '@mui/icons-material/Close';

import { leaveRoom } from '../../../realtimeCommunication/roomHandlers';

const CloseRoomButton = () => {
  return (
    <IconButton onClick={leaveRoom} style={{ color: 'white' }}>
      <CloseIcon />
    </IconButton>
  );
};

export default CloseRoomButton;
