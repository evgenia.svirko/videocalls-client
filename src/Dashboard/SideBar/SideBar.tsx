import { styled } from '@mui/system';

import MainPageButton from './MainPageButton';
import CreateRoomButton from './CreateRoomButton';
import { useAppSelector } from '../../store/store';
import ActiveRoomButton from './ActiveRoomButton';

const MainContainer = styled('div')({
  width: '72px',
  height: '100%',
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'center',
  backgroundColor: '#202225',
});

const SideBar = () => {
  const { isUserInRoom, activeRooms } = useAppSelector((state) => state.room);

  return (
    <MainContainer>
      <MainPageButton />
      <CreateRoomButton isUserInRoom={isUserInRoom} />
      {activeRooms.map((room) => (
        <ActiveRoomButton
          key={room.roomId}
          room={room}
          isUserInRoom={isUserInRoom}
        />
      ))}
    </MainContainer>
  );
};

export default SideBar;
