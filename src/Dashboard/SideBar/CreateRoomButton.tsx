import { FC } from 'react';
import AddIcon from '@mui/icons-material/Add';
import Button from '@mui/material/Button';

import { createNewRoom } from '../../realtimeCommunication/roomHandlers';

type Props = {
  isUserInRoom: boolean;
};

const CreateRoomButton: FC<Props> = ({ isUserInRoom }) => {
  return (
    <Button
      disabled={isUserInRoom}
      onClick={createNewRoom}
      style={{
        width: '48px',
        height: '48px',
        borderRadius: '16px',
        margin: 0,
        padding: 0,
        minWidth: 0,
        marginTop: '10px',
        color: 'white',
        backgroundColor: '#5865F2',
      }}
    >
      <AddIcon />
    </Button>
  );
};

export default CreateRoomButton;
