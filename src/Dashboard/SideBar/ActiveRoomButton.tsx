import { FC } from 'react';
import Button from '@mui/material/Button';
import Tooltip from '@mui/material/Tooltip';

import Avatar from '../../shared/components/Avatar';
import { joinRoom } from '../../realtimeCommunication/roomHandlers';

type Props = {
  room: ActiveRoom;
  isUserInRoom: boolean;
};

const ActiveRoomButton: FC<Props> = ({ room, isUserInRoom }) => {
  const amountOfParticipants = room.participants.length;

  const handleJoinActiveRoom = (): void => {
    if (amountOfParticipants < 4) {
      joinRoom(room);
    }
  };

  const activeRoomButtonDisabled = amountOfParticipants > 3;
  const roomTitle = `Creator: ${room.creatorUsername}. Connected: ${amountOfParticipants}`;

  return (
    <Tooltip title={roomTitle}>
      <div>
        <Button
          style={{
            width: '48px',
            height: '48px',
            borderRadius: '16px',
            margin: 0,
            padding: 0,
            minWidth: 0,
            marginTop: '10px',
            color: 'white',
            backgroundColor: '#5865F2',
          }}
          disabled={activeRoomButtonDisabled || isUserInRoom}
          onClick={handleJoinActiveRoom}
        >
          <Avatar username={room.creatorUsername} />
        </Button>
      </div>
    </Tooltip>
  );
};

export default ActiveRoomButton;
