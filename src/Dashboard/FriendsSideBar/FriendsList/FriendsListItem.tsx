import { FC } from 'react';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';

import Avatar from '../../../shared/components/Avatar';
import OnlineIndicator from './OnlineIndicator';
import { useAppDispatch } from '../../../store/store';
import {
  setChatType,
  setChosenChatDetails,
} from '../../../store/slices/chat/chatSlice';

type Props = {
  id: string;
  username: string;
  isOnline: boolean;
};

const FriendsListItem: FC<Props> = ({ id, username, isOnline }) => {
  const dispatch = useAppDispatch();

  const handleChooseActiveConversation = (): void => {
    dispatch(setChosenChatDetails({ id, name: username }));
    dispatch(setChatType('DIRECT'));
  };

  return (
    <Button
      onClick={handleChooseActiveConversation}
      style={{
        width: '100%',
        height: '42px',
        marginTop: '10px',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'flex-start',
        textTransform: 'none',
        color: 'black',
        position: 'relative',
      }}
    >
      <Avatar username={username} />
      <Typography
        style={{
          marginLeft: '7px',
          fontWeight: 700,
          color: '#8e9297',
        }}
        variant="subtitle1"
        align="left"
      >
        {username}
      </Typography>
      {isOnline && <OnlineIndicator />}
    </Button>
  );
};

export default FriendsListItem;
