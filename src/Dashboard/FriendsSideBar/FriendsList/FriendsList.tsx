import { styled } from '@mui/system';

import FriendsListItem from './FriendsListItem';
import { useAppSelector } from '../../../store/store';

const MainContainer = styled('div')({
  flexGrow: 1,
  width: '100%',
});

const checkOnlineUsers = (
  friends: Friend[],
  onlineUsers: OnlineUser[]
): (Friend & { isOnline: boolean })[] =>
  friends.map((friend) => {
    const isOnline = !!onlineUsers.find(({ userId }) => userId === friend.id);

    return { ...friend, isOnline };
  });

const FriendsList = () => {
  const { friends, onlineUsers } = useAppSelector((store) => store.friends);

  return (
    <MainContainer>
      {checkOnlineUsers(friends, onlineUsers).map(
        ({ username, id, isOnline }) => (
          <FriendsListItem
            key={id}
            id={id}
            username={username}
            isOnline={isOnline}
          />
        )
      )}
    </MainContainer>
  );
};

export default FriendsList;
