import { FC, useEffect, useState } from 'react';
import Dialog from '@mui/material/Dialog';
import DialogTitle from '@mui/material/DialogTitle';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import Typography from '@mui/material/Typography';

import PrimaryButton from '../../shared/components/PrimaryButton';
import InputWithLabel from '../../shared/components/InputWithLabel';
import { validateMail } from '../../shared/utils/validators';
import { useAppDispatch } from '../../store/store';
import { sendInvitation } from '../../store/slices/friends/friendsOperations';

type Props = {
  isDialogOpen: boolean;
  closeDialogHandler: () => void;
};

const AddFriendDialog: FC<Props> = ({ isDialogOpen, closeDialogHandler }) => {
  const [mail, setMail] = useState('');
  const [isFormValid, setIsFormValid] = useState(false);

  const dispatch = useAppDispatch();

  const handleCloseDialog = (): void => {
    closeDialogHandler();
    setMail('');
  };

  useEffect(() => {
    setIsFormValid(validateMail(mail));
  }, [mail, setIsFormValid]);

  const handleSendInvitation = (): void => {
    dispatch(sendInvitation({ targetMail: mail }));
  };

  return (
    <Dialog open={isDialogOpen} onClose={handleCloseDialog}>
      <DialogTitle>
        <Typography>Invite a Friend</Typography>
      </DialogTitle>
      <DialogContent>
        <DialogContentText>
          Enter email address of friend which you would like to invite
        </DialogContentText>
        <InputWithLabel
          label="Mail"
          type="text"
          value={mail}
          setValue={setMail}
          placeholder="Enter mail address"
        />
      </DialogContent>
      <DialogActions>
        <PrimaryButton
          onClick={handleSendInvitation}
          disabled={!isFormValid}
          label="Send"
          additionalStyles={{
            marginLeft: '15px',
            marginRight: '15px',
            marginBottom: '10px',
          }}
        />
      </DialogActions>
    </Dialog>
  );
};

export default AddFriendDialog;
