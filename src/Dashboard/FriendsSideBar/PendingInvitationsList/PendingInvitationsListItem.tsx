import { FC, useState } from 'react';
import Tooltip from '@mui/material/Tooltip';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';

import Avatar from '../../../shared/components/Avatar';
import InvitationDecisionButtons from './InvitationDecisionButtons';
import { useAppDispatch } from '../../../store/store';
import {
  acceptInvitation,
  rejectInvitation,
} from '../../../store/slices/friends/friendsOperations';

type Props = {
  invitationId: string;
  username: string;
  mail: string;
};

const PendingInvitationsListItem: FC<Props> = ({
  invitationId,
  username,
  mail,
}) => {
  const [buttonsDisabled, setButtonsDisabled] = useState(false);

  const dispatch = useAppDispatch();

  const handleAcceptInvitation = (): void => {
    setButtonsDisabled(true);
    dispatch(acceptInvitation({ id: invitationId }));
  };

  const handleRejectInvitation = (): void => {
    setButtonsDisabled(true);
    dispatch(rejectInvitation({ id: invitationId }));
  };

  return (
    <Tooltip title={mail}>
      <div style={{ width: '100%' }}>
        <Box
          sx={{
            width: '100%',
            height: '42px',
            marginTop: '10px',
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'space-between',
          }}
        >
          <Avatar username={username} />
          <Typography
            sx={{
              marginLeft: '7px',
              fontWeight: 700,
              color: '#8e9297',
              flexGrow: 1,
            }}
            variant="subtitle1"
          >
            {username}
          </Typography>
          <InvitationDecisionButtons
            disabled={buttonsDisabled}
            acceptInvitationHandler={handleAcceptInvitation}
            rejectInvitationHandler={handleRejectInvitation}
          />
        </Box>
      </div>
    </Tooltip>
  );
};

export default PendingInvitationsListItem;
