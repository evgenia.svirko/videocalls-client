import { styled } from '@mui/system';

import PendingInvitationsListItem from './PendingInvitationsListItem';
import { useAppSelector } from '../../../store/store';

const MainContainer = styled('div')({
  width: '100%',
  height: '22%',
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'center',
  overflow: 'auto',
});

const PendingInvitationsList = () => {
  const { pendingInvitations } = useAppSelector((store) => store.friends);

  return (
    <MainContainer>
      {pendingInvitations.map(({ _id, senderId: { username, mail } }) => (
        <PendingInvitationsListItem
          key={_id}
          invitationId={_id}
          username={username}
          mail={mail}
        />
      ))}
    </MainContainer>
  );
};

export default PendingInvitationsList;
