import { FC, useEffect } from 'react';
import { styled } from '@mui/system';

import SideBar from './SideBar/SideBar';
import FriendsSideBar from './FriendsSideBar/FriendsSideBar';
import Messenger from './Messenger/Messenger';
import AppBar from './AppBar/AppBar';
import { logout } from '../shared/utils/auth';
import { setUserDetails } from '../store/slices/auth/authSlice';
import { connectWithSocketServer } from '../realtimeCommunication/socketConnection';
import { useAppSelector, useAppDispatch } from '../store/store';
import Room from './Room/Room';

const Wrapper = styled('div')({
  width: '100%',
  height: '100vh',
  display: 'flex',
});

const Dashboard: FC = () => {
  const dispatch = useAppDispatch();

  const { isUserInRoom } = useAppSelector((state) => state.room);

  useEffect(() => {
    const userDetails = localStorage.getItem('user');

    if (!userDetails) {
      logout();
    } else {
      const user = JSON.parse(userDetails) as AuthResponse;

      dispatch(setUserDetails(user));
      connectWithSocketServer(user);
    }
  }, []);

  return (
    <Wrapper>
      <SideBar />
      <FriendsSideBar />
      <Messenger />
      <AppBar />
      {isUserInRoom && <Room />}
    </Wrapper>
  );
};

export default Dashboard;
