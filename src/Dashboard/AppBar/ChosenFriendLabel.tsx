import { Typography } from '@mui/material';

import { useAppSelector } from '../../store/store';

const ChosenFriendLabel = () => {
  const { chosenChatDetails } = useAppSelector((store) => store.chat);

  return (
    <Typography
      sx={{
        fontSize: '16px',
        color: 'white',
        fontWeight: 'bold',
      }}
    >{`${
      chosenChatDetails?.name ? `Chat with ${chosenChatDetails?.name}` : ''
    }`}</Typography>
  );
};

export default ChosenFriendLabel;
