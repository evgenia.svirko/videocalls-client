import { useState } from 'react';
import { PayloadAction } from '@reduxjs/toolkit';
import Menu from '@mui/material/Menu';
import MenuItem from '@mui/material/MenuItem';
import IconButton from '@mui/material/IconButton';
import MoreVertIcon from '@mui/icons-material/MoreVert';

import { logout } from '../../shared/utils/auth';
import { setAudioOnly } from '../../store/slices/room/roomSlice';
import { useAppSelector, useAppDispatch } from '../../store/store';

const DropdownMenu = () => {
  const dispatch = useAppDispatch();
  const { audioOnly } = useAppSelector((state) => state.room);

  const [anchorEl, setAnchorEl] = useState<HTMLButtonElement | null>(null);

  const handleMenuOpen = (
    event: React.MouseEvent<HTMLButtonElement, MouseEvent>
  ): void => setAnchorEl(event.currentTarget);

  const handleMenuClose = (): void => setAnchorEl(null);

  const handleAudioOnlyChange = (): PayloadAction<boolean> =>
    dispatch(setAudioOnly(!audioOnly));

  return (
    <>
      <IconButton onClick={handleMenuOpen} style={{ color: 'white' }}>
        <MoreVertIcon />
      </IconButton>
      <Menu
        id="basic-menu"
        anchorEl={anchorEl}
        open={Boolean(anchorEl)}
        onClose={handleMenuClose}
        MenuListProps={{
          'aria-labelledby': 'basic-button',
        }}
      >
        <MenuItem onClick={logout}>Logout</MenuItem>
        <MenuItem onClick={handleAudioOnlyChange}>
          {audioOnly ? 'Audio Only Enabled' : 'Audio Only Disabled'}
        </MenuItem>
      </Menu>
    </>
  );
};

export default DropdownMenu;
