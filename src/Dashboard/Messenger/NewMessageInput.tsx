import { useState } from 'react';
import { styled } from '@mui/system';

import { sendDirectMessage } from '../../realtimeCommunication/socketConnection';
import { useAppSelector } from '../../store/store';

const MainContainer = styled('div')({
  height: '60px',
  width: '100%',
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
});

const Input = styled('input')({
  backgroundColor: '#2f3136',
  width: '98%',
  height: '44px',
  color: 'white',
  border: 'none',
  borderRadius: '8px',
  fontSize: '14px',
  padding: '0 10px',
});

const NewMessageInput = () => {
  const [message, setMessage] = useState('');

  const { chosenChatDetails } = useAppSelector((store) => store.chat);

  const handleMessageValueChange = (
    event: React.ChangeEvent<HTMLInputElement>
  ): void => setMessage(event.target.value);

  const handleSendMessage = (): void => {
    if (message.length > 0) {
      sendDirectMessage({
        receiverUserId: chosenChatDetails?.id || '',
        content: message,
      });
      setMessage('');
    }
  };

  const handleKeyPressed = (
    event: React.KeyboardEvent<HTMLInputElement>
  ): void => {
    if (event.key === 'Enter') {
      handleSendMessage();
    }
  };

  return (
    <MainContainer>
      <Input
        placeholder={`Write message to ${chosenChatDetails?.name}`}
        value={message}
        onChange={handleMessageValueChange}
        onKeyDown={handleKeyPressed}
      />
    </MainContainer>
  );
};

export default NewMessageInput;
