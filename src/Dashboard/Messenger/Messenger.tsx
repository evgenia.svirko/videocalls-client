import { styled } from '@mui/system';

import { useAppSelector } from '../../store/store';
import MessengerContent from './MessengerContent';
import WelcomeMessage from './WelcomeMessage';

const MainContainer = styled('div')({
  flexGrow: 1,
  backgroundColor: '#36393f',
  marginTop: '48px',
  display: 'flex',
});

const Messenger = () => {
  const { chosenChatDetails } = useAppSelector((store) => store.chat);

  return (
    <MainContainer>
      {!chosenChatDetails ? (
        <WelcomeMessage />
      ) : (
        <MessengerContent chosenChatDetails={chosenChatDetails} />
      )}
    </MainContainer>
  );
};

export default Messenger;
