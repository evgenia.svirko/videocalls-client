import { FC, useEffect } from 'react';
import { styled } from '@mui/system';

import Messages from './Messages/Messages';
import NewMessageInput from './NewMessageInput';
import { getDirectChatHistory } from '../../realtimeCommunication/socketConnection';

type Props = {
  chosenChatDetails: ChatDetails;
};

const Wrapper = styled('div')({
  flexGrow: 1,
});

const MessengerContent: FC<Props> = ({ chosenChatDetails }) => {
  useEffect(() => {
    getDirectChatHistory({
      receiverUserId: chosenChatDetails.id,
    });
  }, [chosenChatDetails]);

  return (
    <Wrapper>
      <Messages />
      <NewMessageInput />
    </Wrapper>
  );
};

export default MessengerContent;
