import { styled } from '@mui/system';

import { useAppSelector } from '../../../store/store';
import MessagesHeader from './MessagesHeader';
import Message from './Message';
import DateSeparator from './DateSeparator';

const MainContainer = styled('div')({
  height: 'calc(100% - 60px)',
  overflow: 'auto',
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'center',
});

const convertDateToHumanReadable = (date: Date, format: string): string => {
  const map: Record<string, string> = {
    mm: String(date.getMonth() + 1),
    dd: String(date.getDate()),
    yy: date.getFullYear().toString().slice(-2),
    yyyy: String(date.getFullYear()),
  };

  return format.replace(/mm|dd|yy|yyy/gi, (matched) => map[matched]);
};

const Messages = () => {
  const { chosenChatDetails, messages } = useAppSelector((store) => store.chat);

  return (
    <MainContainer>
      <MessagesHeader name={chosenChatDetails?.name || ''} />

      {messages.map(({ _id, date, content, author }, index) => {
        const sameAuthor =
          index > 0 &&
          messages[index].author._id === messages[index - 1].author._id;

        const messageDate = convertDateToHumanReadable(
          new Date(date),
          'dd/mm/yy'
        );

        const sameDay =
          index > 0 &&
          messageDate ===
            convertDateToHumanReadable(
              new Date(messages[index - 1].date),
              'dd/mm/yy'
            );

        return (
          <div key={_id} style={{ width: '97%' }}>
            {(!sameDay || index === 0) && <DateSeparator date={messageDate} />}
            <Message
              key={_id}
              content={content}
              author={author.username}
              sameAuthor={sameAuthor}
              sameDay={sameDay}
              date={messageDate}
            />
          </div>
        );
      })}
    </MainContainer>
  );
};

export default Messages;
