import axios, { AxiosError, AxiosRequestHeaders } from 'axios';

import { logout } from './shared/utils/auth';

export const URL = 'https://dry-river-06701.herokuapp.com';
axios.defaults.baseURL = URL;

axios.interceptors.request.use(
  (config) => {
    const userDetails = localStorage.getItem('user');

    if (userDetails) {
      const token = JSON.parse(userDetails).token;

      (config.headers as AxiosRequestHeaders).Authorization = `Bearer ${token}`;
    }

    return config;
  },
  (err) => {
    return Promise.reject(err);
  }
);

export const checkResponseCode = (exception: AxiosError): void => {
  const responseCode = exception?.response?.status;

  if (responseCode) {
    (responseCode === 401 || responseCode === 403) && logout();
  }
};
