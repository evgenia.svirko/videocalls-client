import '@testing-library/jest-dom';
import { rest } from 'msw';
import { setupServer } from 'msw/node';

import { URL } from './api';
import { authStateMock } from './shared/mocks/authStateMock';

const localStorageMock = {
  getItem: jest.fn(),
  setItem: jest.fn(),
  removeItem: jest.fn(),
  clear: jest.fn(),
  length: 5,
  key: () => '',
};

global.localStorage = localStorageMock;

const handlers = [
  rest.post(`${URL}/auth/login`, (_, res, ctx) => {
    return res(ctx.json(authStateMock.user));
  }),
];

const server = setupServer(...handlers);

beforeAll(() => {
  server.listen({ onUnhandledRequest: 'bypass' });
});

afterEach(() => {
  server.resetHandlers();
  localStorage.clear();
});

afterAll(() => {
  server.close();
});
